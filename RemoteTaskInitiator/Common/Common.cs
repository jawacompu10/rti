﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace RemoteTaskInitiator
{
    public static class Common
    {
        private static string currentDirectory;

        public static void Initialize()
        {
            currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            //[TODO] Initialize logger to a new file in currentDirectory. Create with Date in file name.
            string logFilePath = currentDirectory + "\\RemoteTaskInitiatorLog_" + DateTime.Today.ToShortDateString() + ".txt";
            logFilePath = logFilePath.Replace("/", "_");
            Logger.Initialize(logFilePath);
        }

        public static string CurrentDirectory
        {
            get
            {
                return currentDirectory;
            }
        }

        public static byte[] AdditionalEntropyForEncryption
        {
            get
            {
                return new byte[] { 13, 26, 39, 52, 65, 78, 91 };
            }
        }
    }
}
