﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteTaskInitiator;
using System.Net.Mail;

namespace ConfigurationManager
{
    public class Configuration
    {
        private string configPath = Common.CurrentDirectory + "\\Config.xml";
        private XMLReader configReader;
        private static bool instanceCreated = false;

        //XPATH strings
        private const string XPATHCONTACTS = "contacts";
        private const string XPATHRECIPIENTCREDENTIALS = "contacts/recipientcredentials";
        private const string XPATHRECIPIENTMAILADDRESS = "contacts/recipient";
        private const string XPATHRECIPIENTUSEOUTLOOK = "contacts/recipientcredentials/UseOutlook";
        private const string XPATHRECIPIENTDOMAIN = "contacts/recipientcredentials/domain";
        private const string XPATHRECIPIENTUSERID = "contacts/recipientcredentials/username";
        private const string XPATHRECIPIENTPASSWORD = "contacts/recipientcredentials/password";
        private const string XPATHRECIPIENTURI = "contacts/recipientcredentials/uri";
        private const string XPATHADMINISTRATORMAILADDRESS = "contacts/administrator";
        private const string XPATHSENDERMAILADDRESSES = "contacts/senders";
        private const string XPATHSENDERMAILADDRESS = "contacts/senders/sender";
        private const string XPATHMAXALLOWEDRUNTIME = "tasks/maxAllowedRunTime";
        private const string XPATHPROGRAMPATHOFTASK = "tasks/task[@name='{0}']/program";
        //XPATH strings

        //Private constructor - to enable singleton design pattern.
        private Configuration()
        {
            configReader = new XMLReader(configPath);
        }

        public static bool IsValidEmailAddress(string address)
        {
            //The easiest way to find if a string is a valid e-mail address
            //is to send it as the argument to the MailAddress Constructor.
            //If there is no exception, it is a valid e-mail address.
            try
            {
                MailAddress mailID = new MailAddress(address);
            }
            catch (FormatException)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Creates a new instance for the Configuration class.
        /// </summary>
        /// <returns>A new instance of the configuration class</returns>
        /// <exception cref="InvalidOperationException">Occurs when tried to create a second instance</exception>
        public static Configuration CreateInstance()
        {
            if (instanceCreated)
            {
                throw new InvalidOperationException("Only one instance for this class is allowed, and there's already an instance created.");
            }
            else
            {
                return new Configuration();
            }
        }

        public string RecipientMailAddress
        {
            get
            {
                if (!configReader.CheckIfElementExists(XPATHRECIPIENTMAILADDRESS))
                {
                    throw new System.Xml.XmlException("Looks like the recipient e-mail address is not configured.");
                }
                else
                {
                    return configReader.GetValueOfElement(XPATHRECIPIENTMAILADDRESS);
                }
            }
            set
            {
                if (configReader.CheckIfElementExists(XPATHRECIPIENTMAILADDRESS))
                {
                    configReader.SetElementValue(XPATHRECIPIENTMAILADDRESS, value);
                }
                else
                {
                    configReader.AddChildToElement(XPATHCONTACTS, "recipient", value);
                }
            }
        }

        public bool UseOutlook
        {
            get
            {
                if (!configReader.CheckIfElementExists(XPATHRECIPIENTUSEOUTLOOK))
                {
                    throw new System.Xml.XmlException("Looks like the recipient e-mail address is not configured.");
                }
                else
                {
                    try
                    {
                        bool val = Convert.ToBoolean(configReader.GetValueOfElement(XPATHRECIPIENTUSEOUTLOOK));
                        return val;
                    }
                    catch (FormatException)
                    {
                        //Log - Invalid string.
                        //Assume false
                        return false;
                    }
                }
            }
            set
            {
                if (configReader.CheckIfElementExists(XPATHRECIPIENTUSEOUTLOOK))
                {
                    configReader.SetElementValue(XPATHRECIPIENTDOMAIN, value.ToString());
                }
                else
                {
                    configReader.AddChildToElement(XPATHRECIPIENTCREDENTIALS, "UseOutlook", value.ToString());
                }
            }
        }

        public string RecipientDomain
        {
            get
            {
                if (!configReader.CheckIfElementExists(XPATHRECIPIENTDOMAIN))
                {
                    throw new System.Xml.XmlException("Looks like the recipient domain is not configured.");
                }
                else
                {
                    return configReader.GetValueOfElement(XPATHRECIPIENTDOMAIN);
                }
            }
            set
            {
                if (configReader.CheckIfElementExists(XPATHRECIPIENTDOMAIN))
                {
                    configReader.SetElementValue(XPATHRECIPIENTDOMAIN, value);
                }
                else
                {
                    configReader.AddChildToElement(XPATHRECIPIENTCREDENTIALS, "domain", value);
                }
            }
        }

        public string RecipientUserID
        {
            get
            {
                if (!configReader.CheckIfElementExists(XPATHRECIPIENTUSERID))
                {
                    throw new System.Xml.XmlException("Looks like the recipient user ID is not configured.");
                }
                else
                {
                    return configReader.GetValueOfElement(XPATHRECIPIENTUSERID);
                }
            }
            set
            {
                if (configReader.CheckIfElementExists(XPATHRECIPIENTUSERID))
                {
                    configReader.SetElementValue(XPATHRECIPIENTUSERID, value);
                }
                else
                {
                    configReader.AddChildToElement(XPATHRECIPIENTCREDENTIALS, "username", value);
                }
            }
        }

        public string RecipientPassword
        {
            get
            {
                if (!configReader.CheckIfElementExists(XPATHRECIPIENTPASSWORD))
                {
                    throw new System.Xml.XmlException("Looks like the recipient user ID is not configured.");
                }
                else
                {
                    return configReader.GetValueOfElement(XPATHRECIPIENTPASSWORD);
                }
            }
            set
            {
                if (configReader.CheckIfElementExists(XPATHRECIPIENTPASSWORD))
                {
                    configReader.SetElementValue(XPATHRECIPIENTPASSWORD, value);
                }
                else
                {
                    configReader.AddChildToElement(XPATHRECIPIENTCREDENTIALS, "password", value);
                }
            }
        }

        public string RecipientUri
        {
            get
            {
                if (!configReader.CheckIfElementExists(XPATHRECIPIENTURI))
                {
                    throw new System.Xml.XmlException("Looks like the recipient user ID is not configured.");
                }
                else
                {
                    return configReader.GetValueOfElement(XPATHRECIPIENTURI);
                }
            }
            set
            {
                if (configReader.CheckIfElementExists(XPATHRECIPIENTURI))
                {
                    configReader.SetElementValue(XPATHRECIPIENTURI, value);
                }
                else
                {
                    configReader.AddChildToElement(XPATHRECIPIENTCREDENTIALS, "uri", value);
                }
            }
        }

        public string AdministratorMailAddress
        {
            get
            {
                if (!configReader.CheckIfElementExists(XPATHADMINISTRATORMAILADDRESS))
                {
                    throw new System.Xml.XmlException("Looks like the recipient e-mail address is not configured.");
                }
                else
                {
                    return configReader.GetValueOfElement(XPATHADMINISTRATORMAILADDRESS);
                }
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public List<string> SenderMailAddresses
        {
            get
            {
                return configReader.GetValueOfChildren(XPATHSENDERMAILADDRESSES);
            }
            set
            {
                configReader.AddChildrenToElement(XPATHSENDERMAILADDRESSES, "sender", value);
            }
        }

        public TimeSpan TaskDefaultMaxRunTime
        {
            get
            {
                throw new NotImplementedException();
                if (configReader.CheckIfElementExists(XPATHMAXALLOWEDRUNTIME))
                {
                    string maxTime = configReader.GetValueOfElement(XPATHMAXALLOWEDRUNTIME);

                    //Get the last character of the string, this gives the unit of time.
                    char unit = maxTime[maxTime.Length - 1];

                    //Rip off the last char to get the value
                    string timeAsString = maxTime.Substring(0, maxTime.Length - 1);
                    int time;

                    //If the time given is a valid number, convert it to int.
                    try
                    {
                        time = Convert.ToInt32(time);
                    }
                    catch (FormatException)
                    {
                        throw new System.Xml.XmlException("The max time allowed value in the config XML file is not in correct number format");
                    }

                    switch (unit)
                    {
                        //If specified units is minutes
                        case 'm':
                            return new TimeSpan(0, time, 0);
                            break;
                        //If seconds
                        case 's':
                            return new TimeSpan(0, 0, time);
                            break;
                        //If hours
                        case 'h':
                            return new TimeSpan(time, 0, 0);
                            break;
                        //If it is not one of the above
                        default:
                            throw new System.Xml.XmlException("Invalid units for max allowed time in config XML.");
                    }
                }
                else
                {
                    throw new System.Xml.XmlException("The requested element couldn't be found in the config");
                }
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsConfigurationInitialized()
        {
            bool isInitialized = true;

            //Check whether the recipient mail address is configured.
            Logger.LogInfo("Checking whether the Recipient address is configured in the config file...");
            if (!configReader.CheckIfElementExists(XPATHRECIPIENTMAILADDRESS))
            {
                return false;
            }
            else
            {
                if (configReader.GetValueOfElement(XPATHRECIPIENTMAILADDRESS).Trim().Equals(string.Empty))
                {
                    return false;
                }
            }

            //Check whether Admin mail address is configured.
            Logger.LogInfo("Checking whether the Administrator address is configured in the config file...");
            if (!configReader.CheckIfElementExists(XPATHADMINISTRATORMAILADDRESS))
            {
                return false;
            }
            else
            {
                if (configReader.GetValueOfElement(XPATHADMINISTRATORMAILADDRESS).Trim().Equals(string.Empty))
                {
                    return false;
                }
            }

            //Check whether at least one sender has been configured.
            Logger.LogInfo("Checking whether at least one sender address is configured in the config file...");
            if (!configReader.CheckIfElementExists(XPATHSENDERMAILADDRESS))
            {
                return false;
            }
            else
            {
                if (configReader.GetValueOfElement(XPATHSENDERMAILADDRESS).Trim().Equals(string.Empty))
                {
                    return false;
                }
            }

            return isInitialized;
        }

        /// <summary>
        /// Removes a specified sender from the configuration.
        /// </summary>
        public void RemoveSender(string senderAddress)
        {
            throw new NotImplementedException();
        }

        public string GetProgramPathOfTask(string taskName)
        {
            string expandedXpath = String.Format(XPATHPROGRAMPATHOFTASK, taskName);
            if (!configReader.CheckIfElementExists(expandedXpath))
            {
                throw new ApplicationException("A Program path for the specified task couldn't be found.");
            }
            else
            {
                return configReader.GetValueOfElement(expandedXpath);
            }
        }
    }
}
