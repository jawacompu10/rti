﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteTaskInitiator;
using Microsoft.Exchange.WebServices.Data;

namespace MailReader
{
    public class ExchangeMailReader : IMailReader
    {
        private string userId = string.Empty;
        private string password = string.Empty;
        private string uri = string.Empty;
        private string domain = string.Empty;

        private ExchangeService mailService;

        public ExchangeMailReader(string emailAddress, string userId, string password, string uri, string domain)
        {
            //Configure the exchange mail service.
            mailService = new ExchangeService();
            //mailService.Credentials = new WebCredentials(userId, password, domain);
            //mailService.Url = new System.Uri(uri);

            mailService.UseDefaultCredentials = true;
            mailService.AutodiscoverUrl(emailAddress);
        }

        public ExchangeMailReader(string emailAddress, string password)
        {
            throw new NotImplementedException();
        }

        public string Uri
        {
            get
            {
                return uri;
            }
        }

        public string Domain
        {
            get
            {
                return domain;
            }
        }

        public List<RemoteTaskInitiator.Task> GetNewRequests(object config)
        {
            List<RemoteTaskInitiator.Task> newTasks = new List<RemoteTaskInitiator.Task>();

            //Get the recent 10 mails
            MailItem[] newMails = GetUnreadMailFromInbox();

            //Now parse the mails and identify the tasks in them.
            foreach(MailItem mail in newMails)
            {
                //Check if the mail is from an autorized sender.
                if (config.GetType().GetProperty("AdministratorMailAddress").GetValue(config, null).ToString().Equals(mail.From) ||
                    (config.GetType().GetProperty("SenderMailAddresses").GetValue(config, null) as List<string>).Contains(mail.From))
                {
                    //Next - check if the mail is sent for our tool
                    //By inspecting the subject - it should have '[RemoteTask]'
                    if (mail.Subject.Contains("[RemoteTask]"))
                    {
                        //OK. The mail is for us to handle.
                        //Let us now see what the request type is.
                        string taskType = mail.Subject.Replace("[RemoteTask]", "").Trim();
                        
                        //Remove any spaces between words.
                        taskType = taskType.Replace(" ", "");
                        TaskType typeOfTask = TaskType.NewTask;

                        try
                        {
                             typeOfTask = (TaskType)Enum.Parse(typeof(TaskType), taskType);
                        }
                        catch(ArgumentException)
                        {
                            //If an exception is caught, that might mean the mail is not intended for us.
                            ReplyToSender(mail.From, mail.Subject, "The subject of the mail could not be parsed.\n" + 
                                "Please re-check the subject and resend the mail.");

                            //Move on to the next item.
                            continue;
                        }

                        string taskName = mail.Body.Split(new char[] { '\n' })[0];
                        RemoteTaskInitiator.Task taskInMail = new RemoteTaskInitiator.Task(taskName, "");
                        taskInMail.Type = typeOfTask;

                        newTasks.Add(taskInMail);
                    }
                }
            }

            return newTasks;
        }

        public void ReplyToSender(string sender, string subject, string body)
        {
            throw new NotImplementedException();
        }

        public void UpdateCredentials(string userId, string password)
        {
            throw new NotImplementedException();
        }

        public MailItem[] GetUnreadMailFromInbox()
        {
            FindItemsResults<Item> findResults = mailService.FindItems(WellKnownFolderName.Inbox, new ItemView(10));
            ServiceResponseCollection<GetItemResponse> items =
                mailService.BindToItems(findResults.Select(item => item.Id), new PropertySet(BasePropertySet.FirstClassProperties, EmailMessageSchema.From, EmailMessageSchema.ToRecipients));
            return items.Select(item =>
            {
                return new MailItem()
                {
                    From = ((Microsoft.Exchange.WebServices.Data.EmailAddress)item.Item[EmailMessageSchema.From]).Address,
                    Recipients = ((Microsoft.Exchange.WebServices.Data.EmailAddressCollection)item.Item[EmailMessageSchema.ToRecipients]).Select(recipient => recipient.Address).ToArray(),
                    Subject = item.Item.Subject,
                    Body = item.Item.Body.ToString(),
                };
            }).ToArray();
        }

    }

    // Simplified mail item
    public class MailItem
    {
        public string From;
        public string[] Recipients;
        public string Subject;
        public string Body;
    }

}
