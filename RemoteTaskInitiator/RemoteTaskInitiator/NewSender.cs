﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RemoteTaskInitiator
{
    public partial class NewSender : Form
    {
        public NewSender()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if(ConfigurationManager.Configuration.IsValidEmailAddress(textBoxNewSenderAddress.Text))
            {
                //If the e-mail address is valid, add it to the sender's list.
                Dashboard.Configuration.SenderMailAddresses.Add(textBoxNewSenderAddress.Text);
            }
            else
            {
                MessageBox.Show("The e-mail address entered doesn't seem to be a valid one." + 
                    "Please re-check the e-mail address you entered.", "Invalid E-mail", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
