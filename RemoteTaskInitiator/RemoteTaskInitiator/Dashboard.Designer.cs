﻿namespace RemoteTaskInitiator
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelRecipientMailAddress = new System.Windows.Forms.Label();
            this.linkLabelChangeRecipientAddress = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.labelAdminMailAddress = new System.Windows.Forms.Label();
            this.linkLabelChangeAdminAddress = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.listSenderAddresses = new System.Windows.Forms.ListBox();
            this.buttonAddSender = new System.Windows.Forms.Button();
            this.buttonRemoveSender = new System.Windows.Forms.Button();
            this.buttonEditSender = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Californian FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(233, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Remote Task Initiator";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Task Recipient e-mail: ";
            // 
            // labelRecipientMailAddress
            // 
            this.labelRecipientMailAddress.AutoSize = true;
            this.labelRecipientMailAddress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecipientMailAddress.Location = new System.Drawing.Point(195, 77);
            this.labelRecipientMailAddress.Name = "labelRecipientMailAddress";
            this.labelRecipientMailAddress.Size = new System.Drawing.Size(151, 16);
            this.labelRecipientMailAddress.TabIndex = 2;
            this.labelRecipientMailAddress.Text = "<recipient.mail.address>";
            // 
            // linkLabelChangeRecipientAddress
            // 
            this.linkLabelChangeRecipientAddress.AutoSize = true;
            this.linkLabelChangeRecipientAddress.Location = new System.Drawing.Point(490, 79);
            this.linkLabelChangeRecipientAddress.Name = "linkLabelChangeRecipientAddress";
            this.linkLabelChangeRecipientAddress.Size = new System.Drawing.Size(44, 13);
            this.linkLabelChangeRecipientAddress.TabIndex = 3;
            this.linkLabelChangeRecipientAddress.TabStop = true;
            this.linkLabelChangeRecipientAddress.Text = "Change";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Administrator e-mail: ";
            // 
            // labelAdminMailAddress
            // 
            this.labelAdminMailAddress.AutoSize = true;
            this.labelAdminMailAddress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAdminMailAddress.Location = new System.Drawing.Point(195, 120);
            this.labelAdminMailAddress.Name = "labelAdminMailAddress";
            this.labelAdminMailAddress.Size = new System.Drawing.Size(176, 16);
            this.labelAdminMailAddress.TabIndex = 5;
            this.labelAdminMailAddress.Text = "<administrator.mail.address>";
            // 
            // linkLabelChangeAdminAddress
            // 
            this.linkLabelChangeAdminAddress.AutoSize = true;
            this.linkLabelChangeAdminAddress.Location = new System.Drawing.Point(490, 120);
            this.linkLabelChangeAdminAddress.Name = "linkLabelChangeAdminAddress";
            this.linkLabelChangeAdminAddress.Size = new System.Drawing.Size(44, 13);
            this.linkLabelChangeAdminAddress.TabIndex = 6;
            this.linkLabelChangeAdminAddress.TabStop = true;
            this.linkLabelChangeAdminAddress.Text = "Change";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(33, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Sender(s) e-mail: ";
            // 
            // listSenderAddresses
            // 
            this.listSenderAddresses.FormattingEnabled = true;
            this.listSenderAddresses.Location = new System.Drawing.Point(198, 163);
            this.listSenderAddresses.Name = "listSenderAddresses";
            this.listSenderAddresses.Size = new System.Drawing.Size(268, 82);
            this.listSenderAddresses.TabIndex = 8;
            this.listSenderAddresses.SelectedIndexChanged += new System.EventHandler(this.listSenderAddresses_SelectedIndexChanged);
            // 
            // buttonAddSender
            // 
            this.buttonAddSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.buttonAddSender.Location = new System.Drawing.Point(493, 163);
            this.buttonAddSender.Name = "buttonAddSender";
            this.buttonAddSender.Size = new System.Drawing.Size(75, 23);
            this.buttonAddSender.TabIndex = 9;
            this.buttonAddSender.Text = "Add";
            this.buttonAddSender.UseVisualStyleBackColor = true;
            // 
            // buttonRemoveSender
            // 
            this.buttonRemoveSender.Enabled = false;
            this.buttonRemoveSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.buttonRemoveSender.Location = new System.Drawing.Point(493, 192);
            this.buttonRemoveSender.Name = "buttonRemoveSender";
            this.buttonRemoveSender.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveSender.TabIndex = 10;
            this.buttonRemoveSender.Text = "Remove";
            this.buttonRemoveSender.UseVisualStyleBackColor = true;
            this.buttonRemoveSender.Click += new System.EventHandler(this.buttonRemoveSender_Click);
            // 
            // buttonEditSender
            // 
            this.buttonEditSender.Enabled = false;
            this.buttonEditSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.buttonEditSender.Location = new System.Drawing.Point(493, 222);
            this.buttonEditSender.Name = "buttonEditSender";
            this.buttonEditSender.Size = new System.Drawing.Size(75, 23);
            this.buttonEditSender.TabIndex = 11;
            this.buttonEditSender.Text = "Edit";
            this.buttonEditSender.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "History: ";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 275);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(647, 221);
            this.richTextBox1.TabIndex = 13;
            this.richTextBox1.Text = "";
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 508);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonEditSender);
            this.Controls.Add(this.buttonRemoveSender);
            this.Controls.Add(this.buttonAddSender);
            this.Controls.Add(this.listSenderAddresses);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.linkLabelChangeAdminAddress);
            this.Controls.Add(this.labelAdminMailAddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.linkLabelChangeRecipientAddress);
            this.Controls.Add(this.labelRecipientMailAddress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelRecipientMailAddress;
        private System.Windows.Forms.LinkLabel linkLabelChangeRecipientAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelAdminMailAddress;
        private System.Windows.Forms.LinkLabel linkLabelChangeAdminAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listSenderAddresses;
        private System.Windows.Forms.Button buttonAddSender;
        private System.Windows.Forms.Button buttonRemoveSender;
        private System.Windows.Forms.Button buttonEditSender;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

