﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Net.Sockets;
using System.IO;
using RemoteTaskInitiator;

namespace MailReader
{
    public class GMailReader : IMailReader
    {
        TcpClient tcpClient;
        SslStream ssl;
        StreamWriter outfile;
        
        public GMailReader(string username, string password)
        {
            outfile = new StreamWriter(@"D:\Jawahar\gmail_out.txt");
            tcpClient = new TcpClient("imap.gmail.com", 993);
            ssl = new SslStream(tcpClient.GetStream());

            ssl.AuthenticateAsClient("imap.gmail.com");

            outfile.WriteLine("Empty string response: \r\n");
            outfile.WriteLine(receiveResponse(""));
            outfile.WriteLine("=======================================================\r\n");
            outfile.Flush();

            outfile.WriteLine("$ LOGIN " + username + " " + password + "  \r\n");
            outfile.WriteLine(receiveResponse("$ LOGIN " + username + " " + password + "  \r\n"));
            outfile.WriteLine("=======================================================\r\n");
            outfile.Flush();
        }

        public List<Task> GetNewRequests(object config)
        {
            List<Task> newTasks = new List<Task>();

            outfile.WriteLine("$ LIST " + "\"\"" + " \"*\"" + "\r\n");
            outfile.WriteLine(receiveResponse("$ LIST " + "\"\"" + " \"*\"" + "\r\n"));
            outfile.WriteLine("=======================================================\r\n");
            outfile.Flush();

            outfile.WriteLine("$ SELECT INBOX\r\n");
            outfile.WriteLine(receiveResponse("$ SELECT INBOX\r\n"));
            outfile.WriteLine("=======================================================\r\n");
            outfile.Flush();

            return newTasks;
        }

        public void UpdateCredentials(string userId, string password)
        {
            throw new NotImplementedException();
        }

        public void ReplyToSender(string sender, string subject, string body)
        {
            throw new NotImplementedException();
        }

        private string receiveResponse(string command)
        {
            byte[] dummy;
            byte[] buffer;
            int bytes = -1;
            StringBuilder sb = new StringBuilder();

            try
            {
                if (command != "")
                {
                    if (tcpClient.Connected)
                    {
                        dummy = Encoding.ASCII.GetBytes(command);
                        ssl.Write(dummy, 0, dummy.Length);
                    }
                    else
                    {
                        throw new ApplicationException("TCP CONNECTION DISCONNECTED");
                    }
                }
                ssl.Flush();


                buffer = new byte[2048];
                bytes = ssl.Read(buffer, 0, 2048);
                sb.Append(Encoding.ASCII.GetString(buffer));

                return sb.ToString();

            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        } 

    }
}
