﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RemoteTaskInitiator
{
    public static class Logger
    {
        private static StreamWriter logStream = null;
        
        internal static void Initialize(string logFilePath)
        {
            logStream = new StreamWriter(logFilePath, File.Exists(logFilePath));
        }

        private static string GetLongTime()
        {
            return DateTime.Now.ToLongTimeString();
        }

        //[TODO] Add logger methods
        public static void LogInfo(string info)
        {
            if(logStream != null)
            {
                logStream.WriteLine(GetLongTime() + ": [INFO] " + info);
                logStream.Flush();
            }
        }

        public static void LogError(string error)
        {
            if (logStream != null)
            {
                logStream.WriteLine(GetLongTime() + ": [ERROR] " + error);
                logStream.Flush();
            }
        }
    }
}
