﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using MailReader;
using ConfigurationManager;
using RemoteTaskInitiator;

namespace TaskManager
{
    public static class TaskHandler
    {
        private static IMailReader mailClient;
        private static Configuration config;
        private static List<Task> RunningTasks = new List<Task>();

        public static Configuration Configuration
        {
            set
            {
                config = value;
            }
        }
        
        /// <summary>
        /// Checks for new task requests every 10 minutes.
        /// </summary>
        public static void WatchForNewTaskRequests()
        {
            Logger.LogInfo("Initialize the mail client..");
            if (mailClient == null)
            {
                if(config.UseOutlook)
                {
                    Logger.LogInfo("The user has set to use Outllok for accessing the mails...");
                    Logger.LogInfo("Creating Outlook mail client...");
                    mailClient = new OutlookMailReader();
                }
                else if (config.RecipientMailAddress.Contains("gmail"))
                {
                    Logger.LogInfo("The email ID is a Google mail account. Creating gmail client...");
                    mailClient = new GMailReader(config.RecipientUserID, config.RecipientPassword);
                }
                else
                {
                    try
                    {
                        mailClient = new ExchangeMailReader(config.RecipientMailAddress, config.RecipientUserID, config.RecipientPassword, config.RecipientUri, config.RecipientDomain);
                    }
                    catch (UriFormatException)
                    {
                        Thread.CurrentThread.Abort();

                        //[TODO] Add this problem to log, if possible, show a message box.
                        Logger.LogError("The URI specified was inaccessible...");
                    }
                }
            }

            while (true)
            {
                Logger.LogInfo("Reading the mails...");
                List<Task> newTasks = mailClient.GetNewRequests(config);
                if (newTasks.Count > 0)
                {
                    foreach (Task newTask in newTasks)
                    {
                        if(newTask.ProgramToRun == "" || newTask.ProgramToRun == null)
                        {
                            try
                            {
                                newTask.ProgramToRun = config.GetProgramPathOfTask(newTask.TaskName);
                            }
                            catch(ApplicationException)
                            {
                                Logger.LogInfo("Looks like the specified task hasn't been configured to run...");
                                mailClient.ReplyToSender(config.AdministratorMailAddress, "[RemoteTaskInitiator] A non predonfigured task was attempted to run.",
                                    "There was an attempt made to run a task which has not been configured earlier." + 
                                    "\r\nTask name: " + newTask.TaskName + ". Please refer to the app's log for more information.");
                            }
                            
                            //Add code to get the parameters from the config file.
                            //newTask.ParametersRequired = false;
                        }
                        ProcessNewRequest(newTask);
                    }
                }
                else
                {
                    Logger.LogInfo("There were no new task requests found.");
                }

                Thread.Sleep(new TimeSpan(0, 10, 0));
            }
        }

        private static void ProcessNewRequest(Task requestedTask)
        {
            switch (requestedTask.Type)
            {
                case TaskType.NewTask:
                    StartNewTask(requestedTask);
                    break;
                case TaskType.StopTask:
                    break;
                case TaskType.BoostTask:
                    break;
                case TaskType.GetStatus:
                    break;
                case TaskType.GetLog:
                    break;
                default:
                    break;
            }

            
        }

        private static void StartNewTask(Task taskToStart)
        {
            Process startedProcess;

            if(taskToStart.ProgramToRun == "")
            {
                //Get the task's program details from config.
                //config.GetProgramNameOfTask(string taskName);
            }

            if (taskToStart.ParametersRequired)
            {
                ProcessStartInfo processToStart = new ProcessStartInfo(taskToStart.ProgramToRun);
                foreach(string parameter in taskToStart.Parameters)
                {
                    processToStart.Arguments += parameter + " ";
                }
                startedProcess = Process.Start(processToStart);
            }
            else
            {
                startedProcess = Process.Start(taskToStart.ProgramToRun);
            }
            taskToStart.ProcessID = startedProcess.Id;

            RunningTasks.Add(taskToStart);
        }

        private static void StopRunningTask()
        {
            throw new NotImplementedException();
        }

        private static void ReportLongRunningTask()
        {
            throw new NotImplementedException();
        }

        private static void SendTasksStatus()
        {
            throw new NotImplementedException();
        }

        private static void SendLog()
        {
            throw new NotImplementedException();
        }
    }
}
