﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteTaskInitiator
{
    public class Task
    {
        private string taskName = null;
        private string programPath = null;
        private bool parametersRequired = false;
        private List<string> parameters = null;
        private TaskType type = TaskType.NewTask;
        private int maxRunTime = 0;
        private int processId = 0;

        public Task(string taskName, string programPath, List<string> parameters)
        {
            throw new NotImplementedException();
            //Get maxRunTime from configuration
        }

        public Task(string taskName)
        {
            this.taskName = taskName;
        }
        
        public Task(string taskName, string programPath)
        {
            this.taskName = taskName;
            this.programPath = programPath;
            //Get maxRunTime from configuration
        }

        #region Properties

        public string TaskName
        {
            get
            {
                return taskName;
            }
        }

        public string ProgramToRun
        {
            get
            {
                return programPath;
            }
            set
            {
                programPath = value;
            }
        }

        public bool ParametersRequired
        {
            get
            {
                return parametersRequired;
            }
        }

        public List<string> Parameters
        {
            get
            {
                return parameters;
            }
        }

        public TaskType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public int MaxAllowedRunTime
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int ProcessID
        {
            get
            {
                if (processId != 0)
                {
                    return processId;
                }
                else
                {
                    throw new ApplicationException("A process ID for this task couldn't be found. The task is probably not initiated yet.");
                }
            }
            set
            {
                processId = value;
            }
        }

        #endregion Properties
    }

    public enum TaskType
    {
        /// <summary>
        /// Run a new task.
        /// </summary>
        NewTask = 1,
        
        /// <summary>
        /// Stop a running task initiated by the tool.
        /// </summary>
        StopTask,

        /// <summary>
        /// Increase the maximum allowed running time of a task.
        /// </summary>
        BoostTask,

        /// <summary>
        /// Get the status of a task initiated.
        /// </summary>
        GetStatus,

        /// <summary>
        /// Get the log of requests and processes.
        /// </summary>
        GetLog
    }
}
