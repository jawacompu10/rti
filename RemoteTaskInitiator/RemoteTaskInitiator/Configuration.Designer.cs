﻿namespace RemoteTaskInitiator
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.tabContactsTasks = new System.Windows.Forms.TabControl();
            this.tabContacts = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAdministratorMailAddress = new System.Windows.Forms.TextBox();
            this.textBoxRecipientMailAddress = new System.Windows.Forms.TextBox();
            this.buttonEditSender = new System.Windows.Forms.Button();
            this.buttonRemoveSender = new System.Windows.Forms.Button();
            this.buttonAddSender = new System.Windows.Forms.Button();
            this.listSenderAddresses = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabTasks = new System.Windows.Forms.TabPage();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tabContactsTasks.SuspendLayout();
            this.tabContacts.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Californian FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(188, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 24);
            this.label1.TabIndex = 19;
            this.label1.Text = "Remote Task Initiator";
            // 
            // tabContactsTasks
            // 
            this.tabContactsTasks.Controls.Add(this.tabContacts);
            this.tabContactsTasks.Controls.Add(this.tabTasks);
            this.tabContactsTasks.Location = new System.Drawing.Point(12, 60);
            this.tabContactsTasks.Name = "tabContactsTasks";
            this.tabContactsTasks.SelectedIndex = 0;
            this.tabContactsTasks.Size = new System.Drawing.Size(557, 337);
            this.tabContactsTasks.TabIndex = 22;
            // 
            // tabContacts
            // 
            this.tabContacts.Controls.Add(this.textBox4);
            this.tabContacts.Controls.Add(this.label8);
            this.tabContacts.Controls.Add(this.textBox3);
            this.tabContacts.Controls.Add(this.textBox2);
            this.tabContacts.Controls.Add(this.label7);
            this.tabContacts.Controls.Add(this.label6);
            this.tabContacts.Controls.Add(this.textBox1);
            this.tabContacts.Controls.Add(this.label5);
            this.tabContacts.Controls.Add(this.textBoxAdministratorMailAddress);
            this.tabContacts.Controls.Add(this.textBoxRecipientMailAddress);
            this.tabContacts.Controls.Add(this.buttonEditSender);
            this.tabContacts.Controls.Add(this.buttonRemoveSender);
            this.tabContacts.Controls.Add(this.buttonAddSender);
            this.tabContacts.Controls.Add(this.listSenderAddresses);
            this.tabContacts.Controls.Add(this.label4);
            this.tabContacts.Controls.Add(this.label3);
            this.tabContacts.Controls.Add(this.label2);
            this.tabContacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabContacts.Location = new System.Drawing.Point(4, 22);
            this.tabContacts.Name = "tabContacts";
            this.tabContacts.Padding = new System.Windows.Forms.Padding(3);
            this.tabContacts.Size = new System.Drawing.Size(549, 311);
            this.tabContacts.TabIndex = 0;
            this.tabContacts.Text = "Contacts";
            this.tabContacts.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 16);
            this.label6.TabIndex = 33;
            this.label6.Text = "Domain (optional): ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(185, 62);
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '*';
            this.textBox1.Size = new System.Drawing.Size(335, 21);
            this.textBox1.TabIndex = 32;
            this.textBox1.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(168, 16);
            this.label5.TabIndex = 31;
            this.label5.Text = "Recipient e-mail password: ";
            // 
            // textBoxAdministratorMailAddress
            // 
            this.textBoxAdministratorMailAddress.Location = new System.Drawing.Point(162, 165);
            this.textBoxAdministratorMailAddress.Name = "textBoxAdministratorMailAddress";
            this.textBoxAdministratorMailAddress.Size = new System.Drawing.Size(358, 21);
            this.textBoxAdministratorMailAddress.TabIndex = 30;
            // 
            // textBoxRecipientMailAddress
            // 
            this.textBoxRecipientMailAddress.Location = new System.Drawing.Point(162, 27);
            this.textBoxRecipientMailAddress.Name = "textBoxRecipientMailAddress";
            this.textBoxRecipientMailAddress.Size = new System.Drawing.Size(358, 21);
            this.textBoxRecipientMailAddress.TabIndex = 29;
            this.textBoxRecipientMailAddress.MouseHover += new System.EventHandler(this.textBoxRecipientMailAddress_MouseHover);
            // 
            // buttonEditSender
            // 
            this.buttonEditSender.Enabled = false;
            this.buttonEditSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.buttonEditSender.Location = new System.Drawing.Point(451, 271);
            this.buttonEditSender.Name = "buttonEditSender";
            this.buttonEditSender.Size = new System.Drawing.Size(75, 23);
            this.buttonEditSender.TabIndex = 28;
            this.buttonEditSender.Text = "Edit";
            this.buttonEditSender.UseVisualStyleBackColor = true;
            // 
            // buttonRemoveSender
            // 
            this.buttonRemoveSender.Enabled = false;
            this.buttonRemoveSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.buttonRemoveSender.Location = new System.Drawing.Point(451, 241);
            this.buttonRemoveSender.Name = "buttonRemoveSender";
            this.buttonRemoveSender.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveSender.TabIndex = 27;
            this.buttonRemoveSender.Text = "Remove";
            this.buttonRemoveSender.UseVisualStyleBackColor = true;
            // 
            // buttonAddSender
            // 
            this.buttonAddSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.buttonAddSender.Location = new System.Drawing.Point(451, 212);
            this.buttonAddSender.Name = "buttonAddSender";
            this.buttonAddSender.Size = new System.Drawing.Size(75, 23);
            this.buttonAddSender.TabIndex = 26;
            this.buttonAddSender.Text = "Add";
            this.buttonAddSender.UseVisualStyleBackColor = true;
            this.buttonAddSender.Click += new System.EventHandler(this.buttonAddSender_Click);
            // 
            // listSenderAddresses
            // 
            this.listSenderAddresses.BackColor = System.Drawing.Color.White;
            this.listSenderAddresses.FormattingEnabled = true;
            this.listSenderAddresses.ItemHeight = 15;
            this.listSenderAddresses.Location = new System.Drawing.Point(162, 212);
            this.listSenderAddresses.Name = "listSenderAddresses";
            this.listSenderAddresses.Size = new System.Drawing.Size(268, 79);
            this.listSenderAddresses.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 16);
            this.label4.TabIndex = 24;
            this.label4.Text = "Sender(s) e-mail: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 16);
            this.label3.TabIndex = 23;
            this.label3.Text = "Administrator e-mail: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 16);
            this.label2.TabIndex = 22;
            this.label2.Text = "Task Recipient e-mail: ";
            // 
            // tabTasks
            // 
            this.tabTasks.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabTasks.Location = new System.Drawing.Point(4, 22);
            this.tabTasks.Name = "tabTasks";
            this.tabTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tabTasks.Size = new System.Drawing.Size(549, 311);
            this.tabTasks.TabIndex = 1;
            this.tabTasks.Text = "Tasks";
            this.tabTasks.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(370, 403);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 23;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(467, 403);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 24;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 16);
            this.label7.TabIndex = 34;
            this.label7.Text = "Server Uri (optional): ";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(162, 92);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(118, 21);
            this.textBox2.TabIndex = 35;
            this.textBox2.UseSystemPasswordChar = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(162, 123);
            this.textBox3.Name = "textBox3";
            this.textBox3.PasswordChar = '*';
            this.textBox3.Size = new System.Drawing.Size(358, 21);
            this.textBox3.TabIndex = 36;
            this.textBox3.UseSystemPasswordChar = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(286, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 16);
            this.label8.TabIndex = 37;
            this.label8.Text = "User id (optional):";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(402, 92);
            this.textBox4.Name = "textBox4";
            this.textBox4.PasswordChar = '*';
            this.textBox4.Size = new System.Drawing.Size(118, 21);
            this.textBox4.TabIndex = 38;
            this.textBox4.UseSystemPasswordChar = true;
            // 
            // Configuration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 432);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.tabContactsTasks);
            this.Controls.Add(this.label1);
            this.Name = "Configuration";
            this.Text = "Configuration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Configuration_FormClosing);
            this.Load += new System.EventHandler(this.Configuration_Load);
            this.tabContactsTasks.ResumeLayout(false);
            this.tabContacts.ResumeLayout(false);
            this.tabContacts.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabContactsTasks;
        private System.Windows.Forms.TabPage tabContacts;
        private System.Windows.Forms.TextBox textBoxAdministratorMailAddress;
        private System.Windows.Forms.TextBox textBoxRecipientMailAddress;
        private System.Windows.Forms.Button buttonEditSender;
        private System.Windows.Forms.Button buttonRemoveSender;
        private System.Windows.Forms.Button buttonAddSender;
        private System.Windows.Forms.ListBox listSenderAddresses;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabTasks;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label7;
    }
}