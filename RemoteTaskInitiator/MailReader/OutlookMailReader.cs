﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;
using RemoteTaskInitiator;

namespace MailReader
{
    public class OutlookMailReader : IMailReader
    {
        Outlook.Application outlookApp;
        Outlook.MAPIFolder inboxFolder;

        private static string AdministratorEmailAddress;
        private static List<string> SenderEmailAddresses;

        public OutlookMailReader()
        {
            Logger.LogInfo("Logging in to Outlook to the default profile and accessing inbox...");
            outlookApp = new Outlook.Application();
            Outlook.NameSpace oNS = outlookApp.GetNamespace("mapi");
            inboxFolder = oNS.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);

            // Log on by using the default profile or existing session (no dialog box). 
            oNS.Logon("LatestProfile", Type.Missing, true, true);
        }

        public List<Task> GetNewRequests(object config)
        {
            Logger.LogInfo("Getting the administrator e-mail address and the list of sender addresses...");
            AdministratorEmailAddress = config.GetType().GetProperty("AdministratorMailAddress").GetValue(config, null).ToString();
            SenderEmailAddresses = config.GetType().GetProperty("SenderMailAddresses").GetValue(config, null) as List<string>;
            
            List<Task> newTasks = new List<RemoteTaskInitiator.Task>();

            Logger.LogInfo("Fetching the unread mails from inbox...");
            Outlook.Items mails = inboxFolder.Items;
            mails = mails.Restrict("[Unread] = true");
            foreach(Outlook.MailItem mail in mails)
            {
                //IF the mail is from a configured sender.
                Logger.LogInfo("Checking if the mail is from an authorized sender...");
                Logger.LogInfo("Sender: " + mail.Sender.Address);
                string senderEmailAddress = mail.Sender.Address;
                if (senderEmailAddress.Equals(AdministratorEmailAddress) || SenderEmailAddresses.Contains(senderEmailAddress))
                {
                    //Next - check if the mail is sent for our tool
                    //By inspecting the subject - it should have '[RemoteTask]'
                    Logger.LogInfo("The mail is from an authorized sender. Inspecting the subject...");
                    Logger.LogInfo("Mail Subject: " + mail.Subject);
                    if (mail.Subject.Contains("[RemoteTask]"))
                    {
                        //OK. The mail is for us to handle.
                        //Let us now see what the request type is.
                        Logger.LogInfo("Determining the type of the task from the subject...");
                        string taskType = mail.Subject.Replace("[RemoteTask]", "").Trim();

                        //Remove any spaces between words.
                        taskType = taskType.Replace(" ", "");
                        TaskType typeOfTask = TaskType.NewTask;

                        try
                        {
                            typeOfTask = (TaskType)Enum.Parse(typeof(TaskType), taskType);
                        }
                        catch (ArgumentException)
                        {
                            //If an exception is caught, that might mean the mail is not intended for us.
                            Logger.LogError("Looks like the subject does not specify the Task type clearly..");
                            Logger.LogInfo("Sending an email to the sender about this...");
                            ReplyToSender(senderEmailAddress, mail.Subject, "The subject of the mail could not be parsed.\n" +
                                "Please re-check the subject and resend the mail.");

                            //Move on to the next item.
                            Logger.LogInfo("Move on to the next item...");
                            continue;
                        }

                        string taskName = mail.Body.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)[0];
                        Logger.LogInfo("Task name as parsed from the mail: " + taskName);
                        
                        Task taskInMail = new Task(taskName);
                        taskInMail.Type = typeOfTask;

                        //Marking the mail as Read so that it will not be processed by our tool again.
                        Logger.LogInfo("Marking the mail as read...");
                        newTasks.Add(taskInMail);
                        mail.UnRead = false;
                        mail.Save();
                    }
                }
            }

            return newTasks;
        }

        public void ReplyToSender(string sender, string subject, string body)
        {
            throw new NotImplementedException();
        }

        public void UpdateCredentials(string userId, string password)
        {
            throw new NotImplementedException("Outlook client doesn't require credential update in the tool.");
        }
    }
}
