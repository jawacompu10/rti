﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteTaskInitiator;

namespace MailReader
{
    public interface IMailReader
    {
        /// <summary>
        /// Looks for new task request mails.
        /// </summary>
        /// <returns>List of new task requests</returns>
        List<Task> GetNewRequests(object config);

        /// <summary>
        /// Update the user credentials for this e-mail address.
        /// </summary>
        /// <param name="userId">New user ID</param>
        /// <param name="password">New Pasword.</param>
        void UpdateCredentials(string userId, string password);

        /// <summary>
        /// Send a mail to a sender with required info in an unexpected event.
        /// </summary>
        /// <param name="sender">E-mail address of the sender to send the mail to</param>
        /// <param name="subject">Subject of the E-mail message</param>
        /// <param name="body">Contents of the mail</param>
        void ReplyToSender(string sender, string subject, string body);
    }
}
