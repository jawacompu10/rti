﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using ConfigurationManager;

namespace RemoteTaskInitiator
{
    public partial class Configuration : Form
    {
        private const int TOOLTIPTIME = 3 * 1000;

        private bool configurationDone = false;
        
        public Configuration()
        {
            InitializeComponent();
        }

        private void textBoxRecipientMailAddress_MouseHover(object sender, EventArgs e)
        {
            toolTip1.ToolTipTitle = "Recipient e-mail";
            toolTip1.Show("Enter the e-mail address to which you wish to send your task requests", this, TOOLTIPTIME);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult userResponse = MessageBox.Show("Do you want to exit without configuring the tool?\n" +
                "You will not be able to use the tool until you configure it.", "Tool Not Configured", MessageBoxButtons.YesNo);
            
            //If the user selected Yes, exit the app.
            if (userResponse == System.Windows.Forms.DialogResult.Yes)
            {
                Environment.Exit(0);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (AreInputsValid())
            {
                //Updating the configuration.
                //Configure the recipient e-mail address.
                Dashboard.Configuration.RecipientMailAddress = textBoxRecipientMailAddress.Text;

                //Configure the administrator e-mail address.
                Dashboard.Configuration.AdministratorMailAddress = textBoxAdministratorMailAddress.Text;

                //The sender address need not be configured at this point
                //They are already added to the configuration when they're added.

                configurationDone = true;
                this.Close();
            }
        }

        /// <summary>
        /// Validates the inputs entered by the user.
        /// </summary>
        /// <returns>True if the inputs are valid, False if not.</returns>
        private bool AreInputsValid()
        {
            //throw new NotImplementedException("Highlight the field which has invalid data.");
            
            //Check if the recipient address is empty.
            if(textBoxRecipientMailAddress.Text.Trim().Equals(string.Empty))
            {
                MessageBox.Show("Recipient address cannot be empty. Please enter a valid e-mail address.");
                return false;
            }

            //Validate the recipient e-mail address
            if (ConfigurationManager.Configuration.IsValidEmailAddress(textBoxRecipientMailAddress.Text))
            {
                textBoxRecipientMailAddress.ForeColor = Color.Black;
            }
            else
            {
                textBoxRecipientMailAddress.ForeColor = Color.Red;
                return false;
            }

            //Check if the administrator address is empty.
            if (textBoxAdministratorMailAddress.Text.Trim().Equals(string.Empty))
            {
                MessageBox.Show("Administrator address cannot be empty. Please enter a valid e-mail address.");
                return false;
            }
            
            //Validate the administrator e-mail address
            if (ConfigurationManager.Configuration.IsValidEmailAddress(textBoxAdministratorMailAddress.Text))
            {
                textBoxAdministratorMailAddress.ForeColor = Color.Black;
            }
            else
            {
                textBoxAdministratorMailAddress.ForeColor = Color.Red;
                return false;
            }

            //Validate the sender addresses
            //Check if there's at least one sender address added.
            if (listSenderAddresses.Items.Count < 1)
            {
                DialogResult responseNoSenders = MessageBox.Show("You have not added any senders." +
                    "Only the administrator will be able to send tasks to the tool. Do you want to continue?",
                    "No senders added", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if(responseNoSenders == System.Windows.Forms.DialogResult.No)
                {
                    listSenderAddresses.BackColor = Color.RosyBrown;
                    return false;
                }
                else
                {
                    listSenderAddresses.BackColor = Color.White;
                }
            }
            else
            {
                listSenderAddresses.BackColor = Color.White;
            }

            //It is unlikely that there would be a problem here. The values are checked when added.
            foreach (string sender in listSenderAddresses.Items)
            {
                if (!ConfigurationManager.Configuration.IsValidEmailAddress(sender))
                    return false;
            }

            //If the control reaches this point, it means that all the inputs are valid.
            //Hence, return true.
            return true;
        }

        private void Configuration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.Cancel)
            {
                Dashboard.LaunchConfigurationWindow();
            }
            
            DialogResult userResponse = MessageBox.Show("Do you want to exit without configuring the tool?\n" +
                "You will not be able to use the tool until you configure it.", "Tool Not Configured", MessageBoxButtons.YesNo);

            //If the user selected Yes, exit the app.
            if (userResponse == System.Windows.Forms.DialogResult.Yes)
            {
                Environment.Exit(0);
            }
            else if (userResponse == System.Windows.Forms.DialogResult.No)
            {
                //If the user chose No, reopen the window
                //YES, this is a little messy. But this window is already 'closing'.
                this.Hide();
                Configuration_FormClosing(this, new FormClosingEventArgs(CloseReason.UserClosing, true));
            }
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            throw new NotImplementedException("Tasks tab needs to be designed.");
        }

        private void buttonAddSender_Click(object sender, EventArgs e)
        {
            NewSender addSender = new NewSender();
            addSender.ShowDialog();
            
            //Reload the sender addresses to update with the newly added value.
            listSenderAddresses.Items.Clear();
            listSenderAddresses.Items.Add(Dashboard.Configuration.SenderMailAddresses);
        }
    }
}
