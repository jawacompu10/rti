﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace ConfigurationManager
{
    public class XMLReader
    {
        #region Private Members

        private XmlDocument xDoc = new XmlDocument();
        private string documentElementName = null, filePath = null;

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Constructor of the XmlReader class initializing the XmlDocument object.
        /// </summary>
        /// <param name="XMLPath">Path of the XML file to be read.</param>
        public XMLReader(string XMLPath)
        {
            if (File.Exists(XMLPath))
            {
                xDoc.Load(XMLPath);
                documentElementName = xDoc.DocumentElement.Name;
                filePath = XMLPath;
            }
            else throw new FileNotFoundException("The specified XML file doesn't exist", XMLPath);
        }

        /// <summary>
        /// Checks if a requested element exists or not.
        /// </summary>
        /// <param name="xPathToElement">XPATH to the element, omitting the root element</param>
        /// <returns>True/False based on the existence of the element.</returns>
        public bool CheckIfElementExists(string xPathToElement)
        {
            XmlNode targetNode = xDoc.SelectSingleNode(string.Format("/{0}/{1}", documentElementName, xPathToElement));
            return (targetNode != null);
        }

        /// <summary>
        /// Returns the inner text of the specified element.
        /// </summary>
        /// <param name="xPathToElement">XPATH to the element, omitting the root element</param>
        /// <returns>Inner text of the specified element.</returns>
        public string GetValueOfElement(string xPathToElement)
        {
            if(CheckIfElementExists(xPathToElement))
            {
                XmlNode targetNode = xDoc.SelectSingleNode(string.Format("/{0}/{1}", documentElementName, xPathToElement));
                return targetNode.InnerText;
            }
            throw new ArgumentException("The specified element was not found");
        }

        /// <summary>
        /// Gets the value (innertext) of all the children of the specified node
        /// </summary>
        /// <param name="xPathToElement">XPATH to the node the value of whose children is required</param>
        /// <returns>The values of the children of the specified node</returns>
        public List<string> GetValueOfChildren(string xPathToElement)
        {
            if(CheckIfElementExists(xPathToElement))
            {
                XmlNode targetNode = xDoc.SelectSingleNode(string.Format("/{0}/{1}", documentElementName, xPathToElement));
                XmlNodeList requiredChildren = targetNode.ChildNodes;

                //Check if there really are any children for the specified node.
                if(requiredChildren == null || requiredChildren.Count <0)
                {
                    throw new ArgumentException("The specified node doesn't have any children.");
                }
                else
                {
                    List<string> requestedValues = new List<string>();
                    foreach(XmlNode child in requiredChildren)
                    {
                        requestedValues.Add(child.InnerText);
                    }
                    return requestedValues;
                }
            }
            throw new ArgumentException("The specified element was not found");
        }

        public string GetAttributeValue(string xPathToElement, string attributeName)
        {
            throw new NotImplementedException();
            //Remember to save the file after change.
        }

        public Dictionary<string, string> GetAllAttributeValues(string xPathToElement)
        {
            throw new NotImplementedException();
            //Remember to save the file after change.
        }

        public void SetElementValue(string xPathToElement, string value)
        {
            throw new NotImplementedException();
            //Remember to save the file after change.
        }

        public void AddChildrenToElement(string xPathToElement, string childName, List<string> values)
        {
            throw new NotImplementedException();
            //Remember to save the file after change.
        }

        public void AddChildToElement(string xPathToElement, string childName, string value)
        {
            throw new NotImplementedException();
            //Remember to save the file after change.
        }

        public void AddAttributeToElement(string xPathToElement, string attributeName, string attributeValue)
        {
            throw new NotImplementedException();
            //Remember to save the file after change.
        }

        #endregion
    }
}
