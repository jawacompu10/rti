﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ConfigurationManager;
using TaskManager;

namespace RemoteTaskInitiator
{
    public partial class Dashboard : Form
    {
        private static ConfigurationManager.Configuration config;
        public static Thread ListenThread;

        public static ConfigurationManager.Configuration Configuration
        {
            get
            {
                return config;
            }
        }

        public Dashboard()
        {
            //Initialize the common class for it has to be accessed from everywhere.
            Common.Initialize();

            //Create an instance of the configuration manager to access the configuration XML.
            config = ConfigurationManager.Configuration.CreateInstance();

            InitializeComponent();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            //Check whether the configuration is done.
            Logger.LogInfo("Check whether the app's configuration is initialized");
            if (!config.IsConfigurationInitialized())
            {
                //The tool is not configured. Get it configured from the user.
                MessageBox.Show("Looks like this is the first time you are using this tool.\n" +
                    "You will have to configure the tool before you could start using it.", "Configure tool");

                Logger.LogInfo("The tool hasn't been configured already. Loading the configuration window to do so...");
                Configuration configWindow = new Configuration();
                configWindow.ShowDialog();
                
                Logger.LogInfo("Getting the configuratino from the config file and loading it to the Dashboard window.");
                PopulateConfigDataToForm();
            }
            //If the tool is already configured, get the required info to populate the values in the form.
            else
            {
                Logger.LogInfo("Getting the configuratino from the config file and loading it to the Dashboard window.");
                PopulateConfigDataToForm();
            }

            //Once all the initial operations are done, we are ready to go.
            //Start listening for new task requests.
            Logger.LogInfo("Start looking for new task requests...");
            TaskHandler.Configuration = config;
            ThreadStart listenThreadStart = new ThreadStart(TaskHandler.WatchForNewTaskRequests);
            ListenThread = new Thread(listenThreadStart);
            ListenThread.Start();
        }
        
        #region WorkDoers

        public static void LaunchConfigurationWindow()
        {
            Configuration configWindow = new Configuration();
            configWindow.ShowDialog();
        }

        private void PopulateConfigDataToForm()
        {
            labelRecipientMailAddress.Text = config.RecipientMailAddress;
            labelAdminMailAddress.Text = config.AdministratorMailAddress;
            listSenderAddresses.Items.AddRange(config.SenderMailAddresses.ToArray());
        }

        #endregion

        private void listSenderAddresses_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listSenderAddresses.SelectedItems.Count == 1)
            {
                buttonRemoveSender.Enabled = true;
                buttonEditSender.Enabled = true;
            }
            else if (listSenderAddresses.SelectedItems.Count > 1)
            {
                buttonRemoveSender.Enabled = true;
            }
        }

        private void buttonRemoveSender_Click(object sender, EventArgs e)
        {
            if(listSenderAddresses.SelectedItems.Count > 0)
            {
                foreach(string selectedSender in listSenderAddresses.SelectedItems)
                {
                    Configuration.RemoveSender(selectedSender);
                }
            }
        }

    }
}
